<?php

namespace Tonic;

class ForbbidenException extends Exception
{
    protected $code = 403;
    protected $message = 'Forbbiden';

    public function __construct($msg){
        $this->message = $msg;
    }
}
