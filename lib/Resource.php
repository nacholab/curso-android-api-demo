<?php
namespace NachoLab\Lib;

use Tonic\Application;
use Tonic\ConditionException;
use Tonic\Request;
use Tonic;

abstract class Resource extends Tonic\Resource{

    public function __construct(Application $app, Request $request){
        parent::__construct($app,$request);
        $this->dao = $this->getDao();
    }

    protected function checkAction($action,$arg){
        if (strtolower($action)!=strtolower($this->{$arg})) throw new ConditionException();
    }


    protected function checkId($id){
        echo "ID: {$id} -- {$this->id}";
        if (!is_numeric($id) && $id > 0)throw new ConditionException();
    }


    protected abstract function getDao();

}