<?php

namespace Tonic;

class IdIsMissingException extends Exception{
    protected $code = 400;
    protected $message = 'Entity ID is missing or is invalid';
}
