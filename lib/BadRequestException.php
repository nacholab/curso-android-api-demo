<?php

namespace Tonic;

class BadRequestException extends Exception
{
    protected $code = 400;
    protected $message = 'Bad request';

    public function __construct($msg){
        $this->message = $msg;
    }
}
