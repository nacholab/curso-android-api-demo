<?php
namespace NachoLab\Lib;

use DisneyCases\Dao\SystemConfigurationDao;
use DisneyCases\Dao\TemplateDao;

require_once(__DIR__."/rb.php");

abstract class Dao{
    const _ID = "_id";

    const ENTITY_VIDEOGAME = "videogame";

    public static function dbConnect(){
        $dsn = "sqlite:/".__DIR__."/../db/db.sqlite";
        \R::setup($dsn);
    }

    public function prepareAllForClient($objects,$areBeans = FALSE){
        $exportedObjects = array();

        if ($areBeans){
            $exportedObjects = \R::exportAll($objects);
        }

        $preparedObject = array();

        foreach($exportedObjects as $o){
            $preparedObject[] = $this->prepareForClient($o);
        }

        return $preparedObject;
    }

    abstract public function prepareForClient($exportedObject);
}

?>