<?php
namespace CursoAndroid;
?>

<html>
    <head>
        <title>Instalación API CursoAndroid</title>

        <meta charset="utf-8" />

        <style>
            body{
                margin-left:100px;
                margin-right:100px;
                margin-top:50px;
                font-family: sans-serif;
            }

            hr{
                margin-top:10px;
                margin-bottom:10px;
            }
        </style>
    </head>
    <body>

<?php
foreach (array_merge(
        array(
            __DIR__."/../lib/rb.php",
            __DIR__."/../lib/Dao.php"
        ),
        glob(__DIR__."/../Dao/*.php"))
    as $filename){
    echo "Incluyendo {$filename}<br />";
    require_once $filename;
}
echo "<hr />";
use NachoLab\Lib\Dao;

class InitDB{

    public function run(){

        echo "Conectando a la base de datos<br />";
        Dao::dbConnect();

        echo "Limpiando la base de datos<br />";
        \R::nuke();

    }
}

$i = new InitDB();
$i->run();

echo "<hr />";
echo "¡Listo!<br />";
?>
    </body>
</html>