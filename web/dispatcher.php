<?php
include(__DIR__.'/../lib/Tonic/Autoloader.php');

$config = array(
    'load' => array(
        __DIR__.'/../Resources/*.php'
    )
);

foreach (array_merge(
        glob(__DIR__."/../lib/*.php"),
        glob(__DIR__."/../Dao/*.php"))
    as $filename){
    require_once $filename;
}
use Nacholab\Lib\Dao;

Dao::dbConnect();

$app = new Tonic\Application($config);
$request = new Tonic\Request();

try {

    $resource = $app->getResource($request);

    $entity = explode("/",$request->uri)[1];
    if (isset($request->params)){
        if (array_key_exists("id",$request->params)){
            $id = $request->params["id"];
        }else{
            $id = NULL;
        }

        if (array_key_exists("action",$request->params)){
            $resourceAction = $request->params["action"];
        }else{
            $resourceAction = NULL;
        }
    }

    $action = $request->method;

    if ($action != "OPTIONS"){
        $response = $resource->exec();
    }else{
        $response = new \Tonic\Response(\Tonic\Response::OK,NULL);
    }

} catch (Tonic\NotFoundException $e) {
    $response = new Tonic\Response(404, $e->getMessage());

} catch (Tonic\UnauthorizedException $e) {
    $response = new Tonic\Response(401, $e->getMessage());
} catch (Tonic\MethodNotAllowedException $e) {
    $response = new Tonic\Response($e->getCode(), $e->getMessage());
    $response->allow = implode(', ', $resource->allowedMethods());
} catch (Tonic\Exception $e) {
    $response = new Tonic\Response($e->getCode(), $e->getMessage());
}

foreach ($response->headers as $name => $value) {
    header($name.': '.$value, true, $response->code);
}

header("Access-Control-Allow-Origin:*");
header('Access-Control-Allow-Methods:PUT,OPTIONS,GET,POST,DELETE');
header('Allow:GET,POST,PUT,DELETE,OPTIONS');

if ($response->body){
    echo json_encode($response->body);
}