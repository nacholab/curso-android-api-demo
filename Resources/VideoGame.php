<?php
namespace CursoAndroid\Resources;

use CursoAndroid\Dao\VideoGameDao;
use NachoLab\Lib\Auth;
use Tonic\BadRequestException;
use Tonic\NotFoundException;
use Tonic\Response;

/**
 * @uri /videoGame
 * @uri /videoGame/:id
 */
class VideoGame extends BaseVideoGameResource{

    protected function getDao(){
        return new VideoGameDao();
    }

    /**
     * @uri /videoGame/:id
     * @provides application/json
     * @method GET
     */
    public function getVideoGames($id = 0){
        if (is_numeric($id) && $id > 0){
            return $this->getVideoGame($id);
        }else{
            return $this->getAllVideoGames();
        }
    }

    private function getVideoGame($id){
        $vg = $this->dao->getVideoGame($id);
        if ($vg){
            return new Response(Response::OK,$vg);
        }else{
            throw new NotFoundException();
        }
    }

    public function getAllVideoGames(){
        return new Response(Response::OK,$this->dao->getAllVideoGames());
    }

    /**
     * @uri /videoGame/:id
     * @json
     * @provides application/json
     * @method PUT
     */
    public function editVideoGame($id){
        $vg = $this->dao->getVideoGame($id);
        if ($vg){
            return new Response(Response::OK,$this->dao->updateVideoGame($id,$this->sanitizeData($this->request->data)));
        }else{
            throw new NotFoundException();
        }
    }

    private function sanitizeData($data){
        $videoGame = json_decode($data);

        if (!is_object($videoGame)){
            throw new BadRequestException("Malformed JSON");
        }else{
            return $videoGame;
        }
    }

    /**
     * @uri /videoGame/
     * @json
     * @provides application/json
     * @method POST
     */
    public function createVideoGame(){
        return new Response(Response::OK,$this->dao->createVideoGame($this->sanitizeData($this->request->data)));
    }

    /**
     * @uri /videoGame/:id
     * @provides application/json
     * @method DELETE
     */
    public function deleteVideoGame($id){
        $vg = $this->dao->getVideoGame($id);
        if ($vg){
            return new Response(Response::OK,$this->dao->deleteVideoGame($id));
        }else{
            throw new NotFoundException();
        }
    }
}
