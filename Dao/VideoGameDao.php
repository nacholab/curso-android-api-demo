<?php
namespace CursoAndroid\Dao;

use NachoLab\Lib\Dao;

class VideoGameDao extends Dao{

    public function prepareForClient($exportedObject){
        return $exportedObject;
    }

    public function getAllVideoGames(){
        return $this->prepareAllForClient(\R::findAll(Dao::ENTITY_VIDEOGAME),TRUE);
    }

    public function getVideoGame($id){
        $vg = \R::load(Dao::ENTITY_VIDEOGAME,$id);
        if ($vg && isset($vg->id) && $vg->id>0){
            return $this->prepareForClient($vg->export());
        }else{
            return NULL;
        }
    }

    public function updateVideoGame($id,$videoGame){
        return $this->saveVideoGame(\R::load(Dao::ENTITY_VIDEOGAME,$id),$videoGame);
    }

    public function createVideoGame($videoGame){
        return $this->saveVideoGame(\R::dispense(Dao::ENTITY_VIDEOGAME),$videoGame);
    }

    public function deleteVideoGame($id){
        \R::trash(\R::load(Dao::ENTITY_VIDEOGAME,$id));
    }

    private function saveVideoGame($RBVideoGame,$requestedVideoGame){
        $RBVideoGame->import($requestedVideoGame);
        \R::store($RBVideoGame);
        return $this->prepareForClient($RBVideoGame->export());
    }

}