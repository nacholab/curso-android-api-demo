# Llamados

 Recurso    | URL 	| Método	| Descripción 					| Entrada 			|Salida
 :----------|:------|:---------:|:------------------------------|:-----------------:|:----------------:
 videoGame	|		| GET		| Trae todos los videojuegos	|					| [VideoGameDTO]
 videoGame  | /#	| GET		| Trae el videojuego con id #	|					| VideoGameDTO
 videoGame  | 		| POST		| Crea un videojuego 			| VideoGameDTO		| VideoGameDTO
 videoGame  | /#	| PUT		| Edita el videojuego con id #	| VideoGameDTO		| VideoGameDTO
 videoGame  | /#	| DELETE	| Borra el videojuego con id #	| 					|
 
# DTO's

 Nombre     | DTO                                   | Observaciones
 :---------:|:--------------------------------------|:----------------
 VideoGame  |{id:#,name:@,slogan:@,description:@}   |
 
# Glosario

 Ítem   | Significado
 :-----:|:----------------------------
 #      |Valor numérico
 @      |Texto
